package com.sda.collections.queue;

import java.util.ArrayDeque;

public class LifoExample {
    public static void main(String[] args) {
        ArrayDeque<String> documents = new ArrayDeque<>();
        // push/peek/poll
        documents.push("Ion");
        documents.push("Moara cu noroc");
        documents.push("Balatagul");
        System.out.println("Prima carte:"+ documents.peek());

        System.out.println(documents.pop());
        System.out.println(documents.pop());
        System.out.println(documents.pop());
        System.out.println(documents.poll());//null
        System.out.println(documents.pop());//eroare
    }
}
