package com.sda.collections.list;

import java.util.*;

public class ListExample {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        List<String> strings = new LinkedList<>();

        // acestea 2 sunt deprecated si nu le utilizam
        List<String> stack = new Stack<>();
        List<String> vector = new Vector<>();

        list.add(1);//1
        list.add(2);//1,2
        list.add(1,3);//1,3,2
        System.out.println(list);
        list.set(1,4); //1,4,2
        list.remove(1);
        list.remove(Integer.valueOf(1));
//        list.remove(4); - indexOutOfBounds
        System.out.println("Numar elemente "+ list.size());
        System.out.println("Este goala? " + list.isEmpty() );
        System.out.println("Elementul la pozitia 1: " +list.get(0));
        System.out.println("indexul primului: " + list.indexOf(1));
        System.out.println("indexul ultimului 2 din lista:" + list.lastIndexOf(2));

        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            iterator.next();
        }
        List<Integer> integers = Arrays.asList(1, 2, 3);
        Object[] objects = list.toArray();
        System.out.println(list);
//        integers.add(3); - lista din array e immutable

        list.add(1);
        System.out.println(list);
        Collections.sort(list);
        System.out.println(list);
        Collections.sort(list,Comparator.reverseOrder());
        System.out.println(list);
        int i = Collections.binarySearch(list, 1,Comparator.reverseOrder());
        System.out.println(i);
    }
}
