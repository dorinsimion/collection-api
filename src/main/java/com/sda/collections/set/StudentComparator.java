package com.sda.collections.set;

import java.util.Comparator;

public class StudentComparator implements Comparator<Student> {

    @Override
    public int compare(Student o1, Student o2) {
        if(o1.getNume().equals(o2.getNume())){
            return o1.getNota()-o2.getNota();
        } else{
            return o1.getNume().compareTo(o2.getNume());
        }
    }
}
