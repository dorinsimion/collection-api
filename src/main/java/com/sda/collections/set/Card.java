package com.sda.collections.set;

import java.util.Objects;

public class Card {
    private String type;
    private String number;

    public Card(String type, String number) {
        if(type==null || number==null)
            throw new RuntimeException("INVALID!");
        this.type = type;
        this.number = number;
    }

    @Override
    public boolean equals(Object card){
        if (!(card instanceof Card))
            return false;
        Card otherCard =(Card) card;
        return otherCard.type.equals(type)
                && otherCard.number.equals(number);
    }

    @Override
    public int hashCode() {
        return type.hashCode();
    }

    @Override
    public String toString() {
        return type+":"+number;
    }
}
