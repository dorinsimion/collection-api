package com.sda.collections.set;

import java.util.*;

public class SetExample {
    public static void main(String[] args) {
        Set<String> hashSet= new HashSet<>();
        Set<String> linkedSet = new LinkedHashSet<>();//set ordonat
        Set<String> treeSet= new TreeSet<>();//set sortat

        Card card1 = new Card("Romb","POPA");
        Card card2 = new Card("Romb","POPA");
        System.out.println(card1.equals(card2));

        Set<Card> cards = new HashSet<>();
        boolean addFirst = cards.add(card1);
        boolean addSecond = cards.add(card2);
        System.out.println(addFirst+ " " + addSecond);
        System.out.println("Nr elemente "+ cards.size());
        cards.remove(card1);
        System.out.println("Este empty? "+ cards.isEmpty());
        cards.add(new Card("Romb","1"));
        cards.add(new Card("Romb","2"));
        cards.add(new Card("Romb","3"));
        cards.add(new Card("Romb","4"));
        cards.add(new Card("Trefla","2"));
        cards.add(new Card("Inima","5"));
        for(Card card:cards)
            System.out.println(card);

        Set<Student> students = new TreeSet<>();
        students.add(new Student("Ion",4));
        students.add(new Student("Mihai",5));
        students.add(new Student("George",3));

        Set<Student> students2 = new TreeSet<>(new StudentComparator());
        students2.add(new Student("Ion",4));
        students2.add(new Student("Mihai",3));
        students2.add(new Student("George",3));

        System.out.println(students);
        System.out.println(students2);
    }


}
