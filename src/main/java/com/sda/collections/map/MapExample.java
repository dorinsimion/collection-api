package com.sda.collections.map;

import java.util.*;

public class MapExample {
    public static void main(String[] args) {
        Map<String,String> map = new HashMap<>();
        Map<String,Integer> treeMap = new TreeMap<>();

        map.put("Mihai","0758338812");
        map.clear();
        map.put("Ion","0346212232");
        if(map.containsKey("Ion")){
            System.out.println(map.get("Ion"));
        }else{
            System.out.println("Nu contine!");
        }
        map.put("George","0346212232");
        map.put("Ion","0346212232");
        System.out.println("Numar contacte "+ map.size());
        for(String s: map.keySet()){
            System.out.println(s);
        }
        Collection<String> values = map.values();
        for(Map.Entry<String,String> entry:map.entrySet()){
            System.out.println(entry.getKey()+":"+entry.getValue());
        }
        map.put("Ion","Fara numar");
        System.out.println(map.get("Ion"));
        System.out.println(map.size());
        System.out.println(map.isEmpty());
    }
}
